set terminal tikz
set output 'vec.tex'

set datafile separator " "

set xlabel "dimenze"
set x2label "pocet dat /100000"
set x2tics nomirror
set xtics nomirror
set ylabel "speedup"

set xrange [0:32]
set xtics 0,2,32

set x2range [0:500]
set x2tics 0,100,500


set style line 2 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5
set style line 1 \
    linecolor rgb '#dd181f' \
    linetype 1 linewidth 2 \
    pointtype 5 pointsize 1.5

plot 'vec.dat' using 1:2 index 0 with lines linestyle 1 title "dimenze", \
     ''                   index 1 with lines linestyle 2 title "pocet dat" axes x2y1
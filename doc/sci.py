
import time
import numpy as np
from sklearn.neighbors import BallTree

dataset = np.fromfile("uber.tns", dtype=float, count=5*3000000, sep=' ')
dataset = dataset.reshape((3000000, 5))
# print(dataset.shape)
# print(dataset[0:10])

start = time.time()
tree = BallTree(dataset, leaf_size=40)
end = time.time()
print(end - start)
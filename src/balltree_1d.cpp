#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <omp.h>


const int ALIGN = 64;

// typedef struct nodenode_t;
typedef struct node_t {
    double radius;
    double* center;
    node_t* left;
    node_t* right;
} node;

/* Generates pseudo random samples in range 0 - 10 */
int generate_random_data(double* data, long n_samples, int n_features) {

    srandom(0);

    for(long i = 0; i < n_samples*n_features; i++) {
        *data++ =  10 * ((double) random()) / RAND_MAX;
    }

    return 0;
}

/* Returns squared euclidean distance between two points */
double squared_euclidean_distance(double *pt1, double *pt2, int n_dimensions)
{
    double dist = 0.0;
    int d;

    // if (pt1 == pt2)
    //     return dist;
    // #pragma omp simd reduction(+:dist) linear(d)
    for( d = 0; d < n_dimensions; d++)
        dist += (pt1[d] - pt2[d]) * (pt1[d] - pt2[d]);

    return dist;
}

/* Returns euclidean distance between two points */
double euclidean_distance(double *pt1, double *pt2, int n_dimensions) {
    return sqrt(squared_euclidean_distance(pt1, pt2, n_dimensions));
}

/* Returns a pointer to a point furthest from pt */
double* furthest_point(double* data, long n_samples, int n_dimensions, double* pt) {

    double dist = 0, longest = 0;
    double *_pt = NULL;

    for(long i = 0; i < n_samples; ++i) {
        dist = squared_euclidean_distance(pt, data, n_dimensions);

        if (dist > longest) {
            longest = dist;
            _pt = data;
        }
        data += n_dimensions;
    }
    return _pt;
}

double get_radius(double* data, long n_samples, int n_dimensions, double* pt) {
    double dist, longest = 0;

    for(long i = 0; i < n_samples; ++i) {
        dist = squared_euclidean_distance(pt, data, n_dimensions);
        if (dist > longest) {
            longest = dist;
        }
        data += n_dimensions;
    }

    return sqrt(longest);
}

int projection_1d(double* data, double* projection, long n_samples, int n_dimensions, double* a, double* b, double* ab) {

    double ab_ab_product, ap_ab_product;

    for (int d = 0; d < n_dimensions; ++d)
        ab[d] = b[d] - a[d]; 
    
    ab_ab_product = 0;
    for (int d = 0; d < n_dimensions; ++d)
        ab_ab_product += ab[d] * ab[d];

    ab_ab_product = 1/ab_ab_product;

    double *data_ptr = data;
    for (long i = 0; i < n_samples; ++i) {
        ap_ab_product = 0;
        for (int d = 0; d < n_dimensions; ++d)
            ap_ab_product += (*data_ptr++ - a[d]) * ab[d];

        projection[i] = (ap_ab_product * ab_ab_product) * ab[0] + a[0];
    }   

    
    ab[n_dimensions] = ab_ab_product;

    return 0;
}

int point_projection(double* point, double* a, double *ab, int n_dimensions, double* projection) {


    double ap_ab_product = 0;
    for (int d = 0; d < n_dimensions; ++d)
        ap_ab_product += (point[d] - a[d]) * ab[d];

    
    double ab_ab_product = ab[n_dimensions];
    for(int d = 0; d < n_dimensions; ++d)
        projection[d] = (ap_ab_product * ab_ab_product) * ab[d] + a[d];
   
    return 0;
}

int adjust_center(double* center, double* point, double* a, double *ab, int n_dimensions) {

    double ap_ab_product = 0;
    for (int d = 0; d < n_dimensions; ++d)
        ap_ab_product += (point[d] - a[d]) * ab[d];

    for(int d = 0; d < n_dimensions; ++d) 
        center[d] = (((ap_ab_product * ab[n_dimensions]) * ab[d] + a[d]) + center[d]) / 2;

    return 0;   
}

#define SWAP(x,y,tmp) { tmp = x; x = y; y = tmp; }

void swap(double *data, double *projection, long a, long b, int dimensions) {
    
    double tmp;
    SWAP(projection[a], projection[b], tmp);


    double *b_ptr = &data[b*dimensions];
    data += a*dimensions;

    for (int d = 0; d < dimensions; ++d) {
        tmp = data[d];
        data[d] = b_ptr[d];
        b_ptr[d] = tmp;
    }

}

void split_data(double *data, double *projection, long length, int dimensions, long k) {

    long pivot_idx, offset;

    pivot_idx = length-1; // last point
    offset = 0;

    // iterate array and points smaller than pivot move left
    for (long i = 0; i < length-1; ++i) {
        if (projection[i] < projection[pivot_idx]) {
            swap(data, projection, offset, i, dimensions);
            ++offset;
        }
    }

    // move pivot right after elements smaller than pivot
    swap(data, projection, offset, pivot_idx, dimensions);

    pivot_idx = offset;

    if (pivot_idx == k) { // the pivot is also middle point
        return;
    } else if (k < pivot_idx) { // middle point is left from pivot
        split_data(data, projection, offset, dimensions, k);
    } else { // middle element is in the right part of array
        ++offset;
        split_data(data+(dimensions*offset), projection+offset, length-offset, dimensions, k-offset);
    }


}

long find_max(double *projection, long length) {

    double max = 0;
    long max_idx;

    for (long i = 0; i < length; ++i) {
        if (projection[i] > max) {
            max = projection[i];
            max_idx = i;
        }
    }

    return max_idx;

}

node_t* recursive_build(double* data, double* projection, long n_samples, int n_features, node_t* nodes, long* idx) {

    double *a, *a_tmp, *b, *ab;
    long second_middle, local_idx;
    const long middle_idx = n_samples/2;
    node_t* current_node;


    #pragma omp critical
    {
        current_node = nodes + *idx;
        *idx += 1;
        local_idx = *idx;
    }

    // Stop recursion, there is only one point left
    if (n_samples <= 1) {
        memcpy(current_node->center, data, n_features*sizeof(double));
        current_node->radius = 0.0;
        current_node->left = NULL;
        current_node->right = NULL;
        return current_node;
    }

    // Because a could change its place during split_data()
    a = (double*)malloc( n_features * sizeof(double));
    // Will hold vector AB; last place of the array to store dot product ABxAB
    ab = (double*)malloc( (n_features+1) * sizeof(double));
    if (!a | !ab) {
        printf("Allocation error.\n");
        exit(1);
    }

    // Get a-b and compute projections
    a_tmp = furthest_point(data, n_samples, n_features, data);
    memcpy(a, a_tmp, n_features*sizeof(double));
    
    b = furthest_point(data, n_samples, n_features, a);
    
    
    projection_1d(data, projection, n_samples, n_features, a, b, ab);

    // Find middle point in the projection array
    split_data(data, projection, n_samples, n_features, middle_idx);
    point_projection(&data[middle_idx*n_features], a, ab, n_features, current_node->center);

    if (n_samples%2 == 0) {
        second_middle = find_max(projection, middle_idx);
        adjust_center(current_node->center, &data[second_middle*n_features], a, ab, n_features);
    }

    free(ab);
    free(a);


    current_node->radius = get_radius(data, n_samples, n_features, current_node->center);

    // Only if there are enough points, make a local copy to new task
    if (n_samples > 10) {
        double *left_data, *right_data;
        posix_memalign((void**)&left_data,  ALIGN,  middle_idx * n_features * sizeof(double));
        posix_memalign((void**)&right_data, ALIGN,  (n_samples-middle_idx) * n_features * sizeof(double));
        memcpy(left_data, data, middle_idx * n_features*sizeof(double));
        memcpy(right_data, data+(middle_idx*n_features), (n_samples-middle_idx) * n_features*sizeof(double));
        free(data);

        #pragma omp task untied default(shared) final(local_idx > 5) // Just some experiments of impact of final vs if (nothing btw)
        current_node->right = recursive_build(right_data, projection+middle_idx, n_samples-middle_idx, n_features, nodes, idx);
        
        #pragma omp task untied default(shared) final(local_idx > 5)
        current_node->left  = recursive_build(left_data, projection, middle_idx, n_features, nodes, idx);
        
        #pragma omp taskwait
        return current_node;

    } else {
        #pragma omp task untied default(shared) if(local_idx < 10)
        current_node->right = recursive_build(data+(middle_idx*n_features), projection+middle_idx, n_samples-middle_idx, n_features, nodes, idx);

        #pragma omp task untied default(shared) if(local_idx < 10)
        current_node->left  = recursive_build(data, projection, middle_idx, n_features, nodes, idx);
        
        #pragma omp taskwait
        return current_node;
    }

}

/** build_tree() allocates all necessary resources based on shape of the data
  * Then the recursion is called to built the Ball Tree */
node_t* build_tree(double* data, long n_samples, int n_features) {

    double* projection;
    const long n_nodes = 2*n_samples - 1;
    double* center_arr;
    long node_idx = 0;
    node_t* nodes;

    posix_memalign((void**)&projection, ALIGN, n_samples * sizeof(double));
    posix_memalign((void**)&nodes, ALIGN, n_nodes * sizeof(node_t));
    posix_memalign((void**)&center_arr, ALIGN, n_nodes * n_features * sizeof(node_t));

    if (!projection || !nodes || !center_arr) {
        printf("Allocation error.\n");
        exit(1);
    }
    
    // Assign memory for a center to each node
    for (long i = 0; i < n_nodes; ++i)
        nodes[i].center = &center_arr[i * n_features];

    #pragma omp parallel //num_threads(16)
    {
        #pragma omp single
        {
        recursive_build(data, projection, n_samples, n_features, nodes, &node_idx);
        }
    }

    free(projection);

    return nodes;
}


int load_tensor(const char* path, double* data, long n_rows, int n_cols) {

    FILE *file_ptr;
    file_ptr = fopen(path, "r");

    if (!file_ptr) {
        printf("Failed to open tensor %s\n", path);
        exit(1);
    }

    for (long i = 0; i < n_rows*n_cols; ++i) {
        fscanf(file_ptr, "%lf", data+i);
    }

    fclose(file_ptr);

    return 0;
}

void dump_tree(node_t* current_node, long idx, int n_dimensions) {
    if (!current_node) {
        printf("Tree is empty.\n");
        return;
    }

    printf("[%ld] r = %.5f (", idx, current_node->radius);
    for (int d = 0; d < n_dimensions; ++d) {
        printf("%.5f ", current_node->center[d]);
    } printf(")\n");

    if (current_node->left) {
        dump_tree(current_node->left, 2*idx+1, n_dimensions);
        dump_tree(current_node->right, 2*idx+2, n_dimensions);
    }
}

int main(int argc, char *argv[]) {

    double time_start, time_end;
    double *data;
    long n_samples;
    int n_features;
    node_t* tree;

    if(argc < 3) {
        printf("Usage: %s <n_features> <n_samples> [path]\n", argv[0]);
        exit(1);
    }

    n_features = atoi(argv[1]);
    n_samples = atoi(argv[2]);
    if (n_features < 1 || n_samples < 1) {
        printf("Invalid arguments passed.\n");
        exit(1);
    }

    // data = create_2d_array(n_samples, n_features);
    posix_memalign((void**)&data, ALIGN, n_samples * n_features * sizeof(double));
    if (!data) {
        printf("Allocation error.\n");
        exit(1);
    }

    if (argc == 3) generate_random_data(data, n_samples, n_features);
    else load_tensor(argv[3], data, n_samples, n_features);

    time_start = omp_get_wtime();
    tree = build_tree(data, n_samples, n_features);
    time_end = omp_get_wtime();

    fprintf(stderr, "Time: %g\n", time_end-time_start);

    free(data);

    dump_tree(tree, 0, n_features);

    free(tree->center);
    free(tree);

    

    


    return 0;
}

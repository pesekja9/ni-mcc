#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <omp.h>


const int ALIGN = 64;

// typedef struct nodenode_t;
typedef struct node_t {
    double radius;
    double* center;
    node_t* left;
    node_t* right;
} node;

typedef struct {
    double* pt_ptr;
    double  distance;
} pt_pair_t;

typedef struct {
    double value;
    long idx;
} median_pair_t;



/* Returns allocated 2D array */
double** create_2d_array(long n_samples, int n_features) {
    double *_data;
    double **data;

    posix_memalign((void**)&_data, ALIGN, n_samples * n_features * sizeof(double));
    posix_memalign((void**)&data,  ALIGN, n_samples * sizeof(double));

    if (!_data || !data) {
        printf("[ERROR] Allocation was not successfull.\n");
        exit(1);
    }

    for(long i = 0; i < n_samples; i++)
        data[i] = &_data[i * n_features];

    return data;
}

/* Generates pseudo random samples in range 0 - 10 */
int generate_random_data(double** data, long n_samples, int n_features) {

    double *data_ptr;

    srandom(0);

    data_ptr = *data;
    for(long i = 0; i < n_samples*n_features; i++) {
        *data_ptr++ =  10 * ((double) random()) / RAND_MAX;
    }

    return 0;
}

/* Returns squared euclidean distance between two points */
double squared_euclidean_distance(double *pt1, double *pt2, int n_dimensions)
{
    double dist = 0.0;

    for(int d = 0; d < n_dimensions; d++)
        dist += (pt1[d] - pt2[d]) * (pt1[d] - pt2[d]);

    return dist;
}

/* Returns euclidean distance between two points */
double euclidean_distance(double *pt1, double *pt2, int n_dimensions) {
    return sqrt(squared_euclidean_distance(pt1, pt2, n_dimensions));
}


/* Returns a pointer to a point furthest from pt */
double* furthest_point(double** data, long n_samples, int n_dimensions, double* pt) {
    double dist, longest = 0;
    double *_pt = NULL;

    for(long i = 0; i < n_samples; ++i) {
        dist = squared_euclidean_distance(pt, data[i], n_dimensions);
        if (dist > longest) {
            longest = dist;
            _pt = data[i];
        }
    }

    return _pt;
}


double get_radius(double** data, long n_samples, int n_dimensions, double* pt) {

    double dist, longest = 0;

    #pragma omp simd reduction(max:longest)
    for(long i = 0; i < n_samples; ++i) {
        dist = squared_euclidean_distance(pt, data[i], n_dimensions);
        if (dist > longest) {
            longest = dist;
        }
    }

    return sqrt(longest);
}

int projection_1d(double** data, double* projection, long n_samples, int n_dimensions, double* a, double* b, double* ab) {

    double ab_ab_product, ap_ab_product;

    for (int d = 0; d < n_dimensions; ++d)
        ab[d] = b[d] - a[d]; 
    
    ab_ab_product = 0;
    for (int d = 0; d < n_dimensions; ++d)
        ab_ab_product += ab[d] * ab[d];

    ab_ab_product = 1/ab_ab_product;

    for (long i = 0; i < n_samples; ++i) {
        ap_ab_product = 0;
        for (int d = 0; d < n_dimensions; ++d)
            ap_ab_product += (data[i][d] - a[d]) * ab[d];

        projection[i] = (ap_ab_product * ab_ab_product) * ab[0] + a[0];
    }   

    
    ab[n_dimensions] = ab_ab_product;

    return 0;
}

int point_projection(double* point, double* a, double *ab, int n_dimensions, double* projection) {

    double ap_ab_product = 0;
    for (int d = 0; d < n_dimensions; ++d)
        ap_ab_product += (point[d] - a[d]) * ab[d];

    
    double ab_ab_product = ab[n_dimensions];
    for(int d = 0; d < n_dimensions; ++d)
        projection[d] = (ap_ab_product * ab_ab_product) * ab[d] + a[d];
    
    return 0;
}

int adjust_center(double* center, double* point, double* a, double *ab, int n_dimensions) {

    double ap_ab_product = 0;
    for (int d = 0; d < n_dimensions; ++d)
        ap_ab_product += (point[d] - a[d]) * ab[d];

    for(int d = 0; d < n_dimensions; ++d) 
        center[d] = (((ap_ab_product * ab[n_dimensions]) * ab[d] + a[d]) + center[d]) / 2;

    return 0;   
}

void swap(double **data, double *projection, long a, long b) {

    if (a == b) return;
    
    double *tmp_data, tmp_proj;
    
    tmp_proj = projection[a];
    projection[a] = projection[b];
    projection[b] = tmp_proj;

    tmp_data = data[a];
    data[a] = data[b];
    data[b] = tmp_data;

}

void split_data(double **data, double *projection, long length, long k) {

    long pivot_idx, offset;

    pivot_idx = length-1;
    offset = 0;

    // iterate array and points smaller than pivot move left
    for (long i = 0; i < length-1; ++i) {
        if (projection[i] < projection[pivot_idx]) {
            swap(data, projection, offset, i);
            ++offset;
        }
    }

    // move pivot right after elements smaller than pivot
    swap(data, projection, offset, pivot_idx);
    pivot_idx = offset;

    if (pivot_idx == k) { // the pivot is also middle point
        return;
    } else if (k < pivot_idx) { // middle point is left from pivot
        split_data(data, projection, offset, k);
    } else { // middle element is in the right part of array
        ++offset;
        split_data(data+offset, projection+offset, length-offset, k-offset);
    }


}

long find_max(double *projection, long length) {

    double max = 0;
    long max_idx;

    for (long i = 0; i < length; ++i) {
        if (projection[i] > max) {
            max = projection[i];
            max_idx = i;
        }
    }

    return max_idx;

}

node_t* recursive_build(double** data, double* projection, long n_samples, int n_features, node_t* nodes, long* idx);


#pragma omp declare reduction(furthest : pt_pair_t : \
                              omp_out = omp_in.distance > omp_out.distance ? omp_in : omp_out)
                              
#pragma omp declare reduction(max_pt : median_pair_t : \
                              omp_out = omp_in.value > omp_out.value ? omp_in : omp_out)


node_t* recursive_build_par(double** data, double* projection, long n_samples, int n_features, node_t* nodes, long* idx, long depth, int n_threads) {

    pt_pair_t a_pair = {NULL, 0}, b_pair = {NULL, 0};
    double *ab = (double*)malloc((n_features+1) * sizeof(double));
    node_t* current_node;
    const long middle_idx = n_samples/2;
    median_pair_t left_max = {0.0, 0};
    double longest = 0.0;

    #pragma omp critical
    {
        current_node = nodes + *idx;
        *idx += 1;
    }

    if (n_samples <= 1) {
        memcpy(current_node->center, *data, n_features*sizeof(double));
        current_node->radius = 0.0;
        current_node->left = NULL;
        current_node->right = NULL;
        return current_node;
    }

    #pragma omp parallel num_threads(n_threads/depth)
    {
        double *a, *b;
        double dist;

        // Find A
        #pragma omp for reduction(furthest: a_pair) schedule(static)
        for(long i = 0; i < n_samples; ++i) {
            dist = squared_euclidean_distance(data[0], data[i], n_features);
            if (dist > a_pair.distance) {
                a_pair.distance = dist;
                a_pair.pt_ptr = data[i];
            }
        } 
        a = a_pair.pt_ptr;

        // Find B
        #pragma omp for reduction(furthest: b_pair) schedule(static)
        for(long i = 0; i < n_samples; ++i) {
            dist = squared_euclidean_distance(a, data[i], n_features);
            if (dist > b_pair.distance) {
                b_pair.distance = dist;
                b_pair.pt_ptr = data[i];
            }
        }
        b = b_pair.pt_ptr;     

        // Get vec AB + its product
        #pragma omp single
        {
            for (int d = 0; d < n_features; ++d)
                ab[d] = b[d] - a[d];

            double ab_ab_product = 0;
            for (int d = 0; d < n_features; ++d)
                ab_ab_product += ab[d] * ab[d];

            ab[n_features] = 1/ab_ab_product;
        }

        // Get 1d projection
        #pragma omp for schedule(static, 20)
        for (long i = 0; i < n_samples; ++i) {
            double ap_ab_product = 0;
            for (int d = 0; d < n_features; ++d)
                ap_ab_product += (data[i][d] - a[d]) * ab[d];

            projection[i] = (ap_ab_product * ab[n_features]) * ab[0] + a[0];
        }

        // Split data array
        #pragma omp single
        {
            split_data(data, projection, n_samples, middle_idx);
            point_projection(data[middle_idx], a, ab, n_features, current_node->center);
        }

        if (n_samples%2 == 0) {

            // Find max pt in left side
            #pragma omp for reduction(max_pt: left_max) schedule(static)
            for (long i = 0; i < middle_idx; ++i) {
                if (projection[i] > left_max.value) {
                    left_max.value = projection[i];
                    left_max.idx = i;
                }
            }

            #pragma omp single
            {
                adjust_center(current_node->center, data[left_max.idx], a, ab, n_features);
                free(ab);
            }
        
        }

        #pragma omp for reduction(max:longest) schedule(static)
        for(long i = 0; i < n_samples; ++i) {
            dist = squared_euclidean_distance(current_node->center, data[i], n_features);
            if (dist > longest) {
                longest = dist;
            }
        }

        #pragma omp single
        current_node->radius = sqrt(longest);


        

    }

    depth *= 2;

    if ( (n_threads/depth) > 1) {
        current_node->right = recursive_build_par(data+middle_idx, projection+middle_idx, n_samples-middle_idx, n_features, nodes, idx, depth, n_threads);
        current_node->left = recursive_build_par(data, projection, middle_idx, n_features, nodes, idx, depth, n_threads);
    } else {
        // #pragma omp parallel num_threads(n_threads)
        // {
            // #pragma omp single
            current_node->right = recursive_build(data+middle_idx, projection+middle_idx, n_samples-middle_idx, n_features, nodes, idx);

            // #pragma omp single
            current_node->left  = recursive_build(data, projection, middle_idx, n_features, nodes, idx);

        // }
    }

    return current_node;


}

node_t* recursive_build(double** data, double* projection, long n_samples, int n_features, node_t* nodes, long* idx) {

    double *a, *b, *ab;
    long second_middle, local_idx;
    const long middle_idx = n_samples/2;
    node_t* current_node;

    #pragma omp critical
    {
        current_node = nodes + *idx;
        *idx += 1;
        local_idx = *idx;
    }

    // Stop recursion, there is only one point left
    if (n_samples <= 1) {
        memcpy(current_node->center, *data, n_features*sizeof(double));
        current_node->radius = 0.0;
        current_node->left = NULL;
        current_node->right = NULL;
        return current_node;
    }

    // Get a-b
    a = furthest_point(data, n_samples, n_features, data[0]);
    b = furthest_point(data, n_samples, n_features, a);
    
    
    // Will hold vector AB; last place of the array to store dot product ABxAB
    ab = (double*)malloc( (n_features+1) * sizeof(double));
    projection_1d(data, projection, n_samples, n_features, a, b, ab);

    // Find middle point in the projection array
    split_data(data, projection, n_samples, middle_idx);
    point_projection(data[middle_idx], a, ab, n_features, current_node->center);

    if (n_samples%2 == 0) {
        second_middle = find_max(projection, middle_idx);
        adjust_center(current_node->center, data[second_middle], a, ab, n_features);
    }

    free(ab);

    current_node->radius = get_radius(data, n_samples, n_features, current_node->center);

    // if (local_idx < 5) {

        // #pragma omp task untied default(shared) if(local_idx < 10)
        current_node->right = recursive_build(data+middle_idx, projection+middle_idx, n_samples-middle_idx, n_features, nodes, idx);

        // #pragma omp task untied default(shared) if(local_idx < 10)
        current_node->left  = recursive_build(data, projection, middle_idx, n_features, nodes, idx);

        // #pragma omp taskwait
        return current_node;
    // } else {

    //     current_node->right = recursive_build_no_omp(data+middle_idx, projection+middle_idx, n_samples-middle_idx, n_features, nodes, idx);
    //     current_node->left  = recursive_build_no_omp(data, projection, middle_idx, n_features, nodes, idx);
    //     return current_node;
    // }

}

/** build_tree() allocates all necessary resources based on shape of the data
  * Then the recursion is called to built the Ball Tree */
node_t* build_tree(double** data, long n_samples, int n_features) {

    double* projection;
    const long n_nodes = 2*n_samples - 1;
    double* center_arr;
    long node_idx = 0;
    node_t* nodes;

    posix_memalign((void**)&projection, ALIGN, n_samples * sizeof(double));
    posix_memalign((void**)&nodes, ALIGN, n_nodes * sizeof(node_t));
    posix_memalign((void**)&center_arr, ALIGN, n_nodes * n_features * sizeof(node_t));
    
    // Assign memory for a center to each node
    for (long i = 0; i < n_nodes; ++i)
        nodes[i].center = &center_arr[i * n_features];

    // omp_set_nested (1);
    // omp_set_max_active_levels (10);

    // recursive_build_par(data, projection, n_samples, n_features, nodes, &node_idx, 1, omp_get_num_threads());
    recursive_build_par(data, projection, n_samples, n_features, nodes, &node_idx, 1, 16);


    free(projection);

    return nodes;
}


int load_tensor(const char* path, double** data, long n_rows, int n_cols) {

    FILE *file_ptr;
    file_ptr = fopen(path, "r");

    if (!file_ptr) {
        printf("Failed to open tensor %s\n", path);
        exit(1);
    }

    double *data_ptr = *data;
    for (long i = 0; i < n_rows*n_cols; ++i) {
        fscanf(file_ptr, "%lf", data_ptr+i);
    }

    fclose(file_ptr);

    return 0;
}

void dump_tree(node_t* current_node, long idx, int n_dimensions) {
    if (!current_node) {
        printf("Tree is empty.\n");
        return;
    }

    printf("[%ld] r = %.5f (", idx, current_node->radius);
    for (int d = 0; d < n_dimensions; ++d) {
        printf("%.5f ", current_node->center[d]);
    } printf(")\n");

    if (current_node->left) {
        dump_tree(current_node->left, 2*idx+1, n_dimensions);
        dump_tree(current_node->right, 2*idx+2, n_dimensions);
    }
}

int main(int argc, char *argv[]) {
    double time_start, time_end;
    double **data, *_data;
    long n_samples;
    int n_features;
    node_t* tree;

    if(argc < 3) {
        printf("Usage: %s <n_features> <n_samples> [path]\n", argv[0]);
        exit(1);
    }

    n_features = atoi(argv[1]);
    n_samples = atoi(argv[2]);
    if (n_features < 1 || n_samples < 1) {
        printf("Invalid arguments passed.\n");
        exit(1);
    }

    data = create_2d_array(n_samples, n_features);
    if (argc == 3) generate_random_data(data, n_samples, n_features);
    else load_tensor(argv[3], data, n_samples, n_features);

    _data = *data;

    time_start = omp_get_wtime();
    tree = build_tree(data, n_samples, n_features);
    time_end = omp_get_wtime();

    fprintf(stderr, "Time: %g\n", time_end-time_start);

    free(_data);
    free(data);

    dump_tree(tree, 0, n_features);

    free(tree->center);
    free(tree);

    

    


    return 0;
}

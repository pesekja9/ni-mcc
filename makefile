CXX=g++
CXX_FLAGS=-fopenmp -O3 -ftree-vectorize -funsafe-math-optimizations -ffast-math #-fopt-info-vec -fopt-info-vec-missed
CXX_ARCH=-march=ivybridge -mtune=ivybridge
LINK=-lm

all: balltree balltree_1d balltree_dual

balltree:
	$(CXX) $(CXX_FLAGS) $(CXX_ARCH) src/$@.cpp $(LINK) -o $@

balltree_1d:
	$(CXX) $(CXX_FLAGS) $(CXX_ARCH) src/$@.cpp $(LINK) -o $@

balltree_dual:
	$(CXX) $(CXX_FLAGS) $(CXX_ARCH) src/$@.cpp $(LINK) -o $@

clean:
	rm balltree balltree_1d balltree_dual
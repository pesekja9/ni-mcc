\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage[czech]{babel} % základní podpora pro češtinu, mj. správné dělení slov
\usepackage[utf8]{inputenc} % vstupní kódování je UTF-8
\usepackage[T1]{fontenc} % výstupní kódování
\usepackage{fontspec} % řeší zahrnutí správných fontů pro LuaLaTeX a XeLaTeX
\usepackage{tikz}
\usepackage{gnuplot-lua-tikz}
\usepackage{fancyvrb}


%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{natbib}
\bibliographystyle{unsrt}
%% Title
\title{
		\usefont{OT1}{bch}{b}{n}
		\normalfont \normalsize \textsc{Semestrální technická zpráva k předmětu NI-MCC} \\ [10pt]
		\huge Algoritmus Ball Tree \\
}

\usepackage{authblk}
\author{Jan Pešek}


\begin{document}
\maketitle

\begin{abstract}
Tato práce je doprovodnou technickou zprávou k semestrální práci předmětu
NI-MCC, konkrétně se zabývá algoritmem pro konstrukci datové struktury nazvané
jako Ball Tree. Jedná se o alternativu ke známější stromové struktuře KD Tree.
Práce diskutuje jednotlivé kroky vedoucí ke zrychlení běhu 
algoritmu pro konstrukci Ball Tree.
\end{abstract} 

\section{Úvod}
Problém hledání nejbližších sousedů bodů ve více rozměrném prostoru je součástí
mnoha algoritmů strojového učení. Scikit-learn implementuje tři algoritmy, které
tento problém řeší -- Brute Force, K-D Tree a Ball Tree. Ve všech případech je
cílem uspořádat více dimenzionální data pro rychlé vyhledávání nejbližšího
souseda. Zatímco konstrukce KD Tree štěpí data pouze přes osy prostoru,
konstrukce Ball Tree probíhá rekurzivně přes hyperkoule, čímž se stává
výhodnější pro vysoko dimenzionální data\cite{scikitNN}.

Ball Tree je binární strom, jehož uzly obsahují střed hyperkoule a její poloměr,
a definuje tak prostor obsažený v daném podstromu. Prohledávání stromu je pak
vhodné provést algoritmem DFS. Pokud nalezený uzel definuje hyperkouli, jejíž
prostor nezahrnuje referenční bod, pak není zahrnut v celém podstromu. Postupné
prořezávání tedy binárně půlí hledaný prostor a umožňuje hledání nejbližšího
souseda v logaritmickém čase\cite{wikiBT}. Stavba struktury Ball Tree probíhá
v asymptotickém čase $\mathcal{O}(n\log{}n)$. Sestavením struktury lze tak
získat velmi efektivní řešení problému i pro velké datasety.

\subsection{Popis algoritmu}

Existuje více implementací tohoto algoritmu\cite{const}, tato práce implementuje stavbu
Ball Tree z off-line dat\cite{cornell}.

Algoritmus pracuje nad množinou bodů v prostoru, přičemž bodem je $n$-tice reálných čísel,
kde $n$ značí dimenzi prostoru. Pro ilustraci celého procesu uvažujme pět bodů na rovině
tak, jak je naznačeno na obrázku~\ref{fig:points}.
\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/3}{!}{\input{figures/points.tex}}
    \caption{Vygenerované body v prostoru}
    \label{fig:points}
\end{figure}

Dalším krokem je nalézt dva body $a$, $b$ takové, že jejich vzdálenost je nejvyšší možná.
Protože operace, která by tento požadavek zaručila je výpočetně náročná, je tento krok
aproximován. Nejdříve se zvolí náhodný bod v prostoru $p$, poté se nalezne bod $a$, jako
nejvzdálenější bod od $p$ a bod $b$, který je nejvzdálenější od $a$.

\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/3}{!}{\input{figures/ball1.tex}}
    \caption{První hyperkoule}
    \label{fig:ball1}
\end{figure}

Nalezené body tvoří úsečku $ab$ (ilustrováno na obrázku \ref{fig:ball1} červeně).
Všechny body jsou pak na tuto přímku promítnuty ortogonální projekcí. Bod $p$
je na $ab$ promítnut postupem:
\begin{equation*}
    p = \frac{\overrightarrow{ap} \cdot \overrightarrow{ab}}{\overrightarrow{ab} \cdot \overrightarrow{ab}} ~ \overrightarrow{ab} + a
\end{equation*}
Prostřední bod, nebo také medián (průměr dvou prostředních bodů v případě sudého počtu)
obrazů je nadále považován za střed hyperkoule.
Její poloměr odpovídá vzdálenosti středu od nejvzdálenějšího bodu v prostoru.
Body v prostoru jsou poté rozděleny do dvou skupin v závislosti na tom, zda
jejich obrazy leží na $ab$ vůči středu vpravo, nebo vlevo.

Výše popsaný postup je rekurzivně opakován na dvou vzniklých polovinách prostoru,
jak ilustrují obrázky níže. Rekurze je ukončena v momentě, kdy je hyperkoule
tvořena právě jedním bodem. Pro tento koncový případ je poloměr nulový a za
střed se považuje bod samotný.


\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/3}{!}{\input{figures/ball2.tex}}
\end{figure}

\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/3}{!}{\input{figures/ball3.tex}}
    \caption{Rekurze nad vzniklými podprostory}
    \label{fig:ball2}
\end{figure}

\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/2}{!}{\input{figures/tree.tex}}
    \caption{Vzniklý BallTree}
    \label{fig:tree}
\end{figure}

Tímto postupem vznikne stromová struktura z obrázku \ref{fig:tree},
která umožňuje logaritmické prohledávání prostoru pro nalezení
nejbližšího sousedního bodu.


\subsection{Implementace}

Implementace předpokládá, že všechna data, včetně informaci o
jejich počtu a rozměru, jsou známa před během algoritmu. 
To přináší výhody v alokaci prostředků bez nutnosti realokace,
či postupné alokace v průběhu rekurze. Data jsou uložena v 
jednorozměrném poli za sebou
$[x_{11}, x_{12}, ..., x_{1d}, ..., x_{n1}, x_{n2}, ..., x_{nd} ]$,
kde $n$ značí počet dat a $d$ odpovídá jejich dimenzi.
V průběhu výpočtu jsou vždy použity všechny souřadnice
bodu najednou, úvaha nad jiným uspořádáním pole je tedy zbytečná.

Za diskusi však stojí způsob přístupu k poli a to buď
vytvořením druhého pole délky $n$ s ukazateli na první
dimenzi příslušného bodu, nebo mapovací funkcí.
První způsob přináší zrychlení při manipulaci s obsahem
pole, respektive prohození prvků znamená prohození ukazatelů.
Nevýhodou se zdá být, že spodní pole zůstává ve stejném
pořadí a lineární průchod daty po promíchání ukazatelů
bude ve skutečnosti nesouvislý.
Při použití mapovací funkce musíme udržovat pořadí datového
pole a prohození dvou prvků je tedy závislé na dimenzi dat.
Toto dilema je vhodné vyřešit experimentálně. Tento experiment
byl proveden nad vygenerovanými daty s fixním počtem 1 milion,
naopak rozměr dat byl upravován pro zjištění závislosti
na době běhu při různém přístupu k poli.\footnote{S kompilací
gcc -O3 -march=ivybridge}
\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/2}{!}{\input{figures/dVs2d}}
    \caption{Měření způsobů přístupu k poli}
    \label{fig:dim}
\end{figure}
Obrázek \ref{fig:dim} vychází z měření na klastru STAR a ukazuje,
že při vysoké dimenzionalitě ($d > 100$) je výrazně účinější přístup
přes ukazatele. Doba výpočtu s nízkou dimenzionalitu nevykazuje významý
spread. 
Dlužno dodat, že na svém notebooku byla situace naprosto odlišná,
Přímý přístup zaznamenával poloviční dobu běhu pro nižší rozměry a
zandebatelný rozdíl pro vyšší rozměry. Zde nejspíš hraje roli
velikost cache klasteru, která tlumí důsledky úpravy pouze na úrovni
ukazatelů.

Díky dopředné znalosti počtu dat, lze také určit velikost výsledného
stromu. Platí, že počet uzlů se rovná $2n - 1$. Strom je reprezentován
jako pole struktur. V tomto případě nedochází ke zbytečným výpadkům paměti,
protože obsah uzlů je vždy zapisován nebo čten celý. 

Stavba stromu je rekurzivní proces. Pro nalezení dvou nejvzdálenějších bodů,
je potřeba dvakrát prohledat celý prostor. Pro porovnání dvou vzdáleností
není potřeba znát přesnou euklidovskou vzdálenost, stačí porovnat jejich
druhé mocniny, což značně urychlí tento proces. Podobná optimalizace nastává
i v případě ortogonální projekce. Pro rozdělení bodů totiž stačí znát
obraz jen jedné dimenze, mezi kterou pak probíhá porovnávání. Teprve až
pro získání středu hyperkoule je nutné dopočítat zbylé dimenze.

Pro nalezení prostředního promítnutého bodu, respektive pro rozpůleni
dat je použit algoritmus QuickSelect, tedy algoritmus pro nalezení
$k$-tého nejmenšího prvku. Pro BallTree to znamená volbu $k=\lfloor n/2 \rfloor$.
Rychlost quickselectu je závislá na volbě pivota. Proto bylo provedeno
měření doby běhu BallTree algoritmu v závislosti na metodě volbě pivota.
Sběr časových dat byl proveden nad volně dostupnými daty Uber Pickups\cite{uber}
o rozměru $5\times 3\,000\,000$. První metoda volí pivota vždy jako
poslední prvek pole, druhá volí pivota jako medián tří hodnot z pole,
třetí metoda je známá pod názvem Tukey's ninther\cite{dlang}. Referenční
čas, při použití statické volby prvku, je $3.892 s$. Metoda
výběr ze tří vykazovala variaci od pěti sekund do 25 sekund, v závislosti
na poloze zvolených prvků v poli. Výsledky měření ninther byly ještě
vzdálenější od referenční časové hodnoty. V případě BallTree algoritmu
je quickselect přetěžovaný, a proto se zde projeví režie přípravy,
obzvláště pak, když vzorky leží v poli daleko od sebe.

Po tomto kroku budou data rozdělena do dvou skupin -- vlevo a vpravo 
od prostředního obrazu. Ještě je potřeba ošetřit případ sudosti
prvků a neexistence prostředního. V tom případě je z levé části
nalezeno maximum (nejbližší levý soused dělícího prvku) a za
střed je považován aritmetický průměr těchto dvou prostředních bodů.
Poloměr hyperkoule je určen přesnou euklidovskou vzdáleností
nejvzdálenějšího bodu od středu. Algoritmus dále rekurzivně
pokračuje nad dvěma vzniklými skupinami bodů.


\section{Optimalizace}

Implementace algoritmu pro konstrukci BallTree popsána v předchozí
kapitole byla tvořena tak, aby přechod na paralelní nevyžadoval
žádné další zásadní úpravy.

\subsection{Vektorizace}

Hlavní kroky algoritmu jsou tvořeny rekurzivní funkcí, strom se tak
vytváří "nejdříve do hloubky". Tento druh rekurze by bylo možné přepracovat
na iterativní funkci a vytvářet tak strom "do šířky". Toto řešení
by však vyžadovalo explicitně udržovat frontu podproblémů, v případě
více vláknového běhu navíc s exkluzivním přístupem. I tak by tento
iterativní přístup nešel vektorizovat, protože by obsahoval nevektorizovatelné
operace uvnitř těla. Zaměřme se proto na jednodušší smyčky, zajišťující
dílčí úkoly.

Snadno vektorizovatelné jsou smyčky běžící přes dimenze bodů, jejich tělo
je vždy tvořeno nějakou snadnou operací. Lze tedy očekávat, nejvyšší
zrychlení bude zaznamenáno u dat, jejichž dimenze je násobkem čtyř.
Smyček tohoto druhu je v průběhu výpočtu značné množství.

\begin{figure}[!h]
    \centering
    \begin{BVerbatim}
for (int d = 0; d < n_dimensions; ++d)
    ab[d] = b[d] - a[d]; 

...loop vectorized using 32 byte vectors
    \end{BVerbatim}
\end{figure}   



Dalším typem smyčky je průchod přes počet bodů, která se vyskytuje
v případě hledání nejvzdálenějšího bodu a v případě \verb|1d| projekce.
\begin{figure}[!h]
    \centering
    \begin{BVerbatim}
for(long i = 0; i < n_samples; ++i) {
    dist = d(pt0, pt1);
    if (dist > longest)
        longest = dist;
}

...missed: couldn't vectorize loop
...not vectorized: control flow in loop
    \end{BVerbatim}
\end{figure}

Kvůli obsahu podmínky uvnitř těla nemůže být smyčka automaticky vektorizována,
jak výpis naznačuje. To lze však změnit přidáním řádku:
\verb|#pragma omp simd reduction(max:longest)|, který automatickou
vektorizaci umožní.

\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/2}{!}{\input{figures/vec}}
    \caption{Zrychleni vektorizací}
    \label{fig:vec}
\end{figure}

Jak bylo dříve v textu naznačeno, hlavní zrychlení přináší vektorizace,
při průchodu napříč dimenzemi. To potvrzuje měření zrychlení vektorizace
oproti zakázané automatické vektorizaci zachycené v grafu
na obrázku \ref{fig:vec}. Měření probíhalo na vygenerovaných datech.
Modrá křivka uvádí zrychlení při zafixované dimenzi na 4 a zvyšujícím
počtu dat, červená křivka naopak vznikla fixací počtu dat a upravováním
dimenzionality dat. A skutečně, v případě kdy je počet dimenzí dat
násobkem čtyř, je zaznamenáno až dvojnásobné zrychlení. Počet dat
na účinnost vektorizace velký vliv nemá.


\subsection{Paralelizace}

Algoritmus pro konstrukci struktury BallTree je náročný na paměťové
operace. Udržuje se datové pole velikost $n\times d$, pole obrazů
prvních dimenzí velikosti $n$ a pole struktur pro reprezentaci stromu
o velikosti $2n-1$. Algoritmus je z jeho podstaty stavěn tak, že konstrukce
každé hyperkoule vyžaduje čtyři po sobě jdoucí lineární průchody datovým
polem a vykonání algoritmu quickselect s průměrnou složitostí $\theta(n)$.
To lze považovat za velice nepříznivé z pohledu výpadků paměti. Jedinou
přístupnou optimalizací je loop reversal, která ale při vysokých rozměrech 
dat nezpůsobí výrazné zrychlení.

Jádrem implementace je rekurzivní funkce, přičemž každá úroveň tvoří
dva podproblémy. Ihned se tedy nabízí využití mechanismu OpenMP Task,
jehož zavedení si nežádá úpravy již existujícího kódu. První volání
funkce \verb|recursive_build()| bude provedeno právě jedním vláknem
v paralelním regionu. Vnoření do hlubší rekurze pak bude předcházet
\verb|#pragma omp task|, následovaná \verb|#pragma omp taskwait|.
Od mechanismu task pro funkcionální paralelizaci lze očekávat rovnoměrné
rozdělení zátěže mezi dostupná vlákna. Každý nový task bude
zařazen do poolu, odkud budou vlákna práci přebírat. První měření
však ukazují naprosto opačný trend, s přibývajícími vlákny naopak
výpočetní čas roste. To má na svědomí režie správy tasků, jejichž
počet roste exponenciálně. Mechanismus byl proto upraven tak, aby
se při deseti vytvořených uzlech stromu přestaly nové tasky generovat.
Měření zrychlení oproti sekvenční verzi (červená křivka obrázku \ref{fig:par})
bylo provedeno na Uber Pickup datech a ukazuje výrazné zlepšení
při běhu na dvou a čtyřech jádrech, využití více jader pak ale
vede k obrovské degradaci zrychlení a běh paralelní verze na
šestnácti jádrech je dokonce pomalejší než sekvenční. 

\begin{figure}[!h]
    \centering
    \resizebox{\textwidth/2}{!}{\input{figures/parallel}}
    \caption{Zrychleni paralelizací}
    \label{fig:par}
\end{figure}

Zde za problém považuji výše zmíněné náročné paměťové operace.
V jednom časovém okamžiku totiž 16 různých vláken pracuje
nad jedním velkým polem. Nijak tomu ale nepomohla implementace
za pomocí přímého přístupu k datovému poli (bez ukazatelů),
a tedy s možností přiřadit tasku soukromou lokální kopii
části pole (modrá křivka obrázku \ref{fig:par}).

Velký počet jader ve spojení s popsaným využitím mechanismu
task má také za následek, že v prvních úrovních rekurze
je většina vláken nečinných. Napříkad právě všech 16 vláken
začne být využíváno až ve čtvrté vrstvě rekurze a první
uzel je tvořen nad celým vstupním datasetem sekvenčně.
Pro odstranění tohoto problému byla vytvořena druhá verze
rekurzivní funkce -- \verb|recursive_build_par()|, která
využívá datový paralelismus, tedy konstrukt
\verb|#pragma omp for|, při každém lineárním průchodu dat.
Toto řešení si vyžádalo tvorbu vlastních redukcí pro
získání nejvzdálenějších bodů. Tato
nová funkce zahajuje stavbu stromu. Na počátku vytvoří
paralelní region v závislosti na dostupných datech a
míry vnoření rekurze, aby bylo zajištěno rovnoměrné
rozdělení vláken. Na konci funkce se znovu zavolá
funkce s datovým paralelismem jen pokud by pro další
tvorbu podstromu bylo k dispozici více než jedno vlákno.
Pokud hloubka stromu dosáhla hranice, že se může zapojit
každé vlákno při funkcionálním paralelismu, využije se
dříve existující \verb|recursive_build()|. Zjednodušený
kód této funkcionality zobrazuje kód níže. 
Bohužel tento
postup přinesl jen drobné zlepšení pro více jader (viz
zelená křivka obrázku \ref{fig:par}).

\begin{figure}[!h]
    \centering
    \begin{BVerbatim}
#pragma omp parallel\
    num_threads(n_threads/depth) {...}
    ...
depth *= 2;
if ( (n_threads/depth) > 1) {
    R = recursive_build_par();
    L = recursive_build_par(); }
else {
    R = recursive_build();
    L = recursive_build();}
    \end{BVerbatim}
    \caption{Logika výběru funkce}
\end{figure}



\section*{Závěr}
Tato práce popsala algoritmus a implementaci konstrukci
BallTree v jazyce C. Na závěr bylo vytvořené dílo rychlostně
porovnáno s knihovnou scikit learn\cite{scikitNN}, respektive třídou
\verb|sklearn.neighbors.BallTree|. Python knihovna
byla v průměru čtyřikrát pomalejší.

Sekvenční implementace byla na několika místech upravena tak,
aby nebyly prováděny operace bez další hodnoty. Ukázalo se,
že vektorizace značně zrychlí běh programu v případech, kdy
dimeze dat je násobek čtyř.

Naopak využití více vláken neznamenalo pro tuto implementaci
velký přínos. Tento algoritmus je stavěn na práci s velkým
množstvím dat, což si žádá velkou spotřebu paměťového prostoru.
Při stavbě stromu je nutné lineárně procházet celý rozsah
dat, což způsobuje neoptimální využití paměti. Tento problém
je zvlášť patrný při využití více jader, které najednou potřebují
velké kusy různých paměťových bloků. Řešením by bylo rozdělit
datový prostor do menších částí tak, aby se nepracovalo s velkým
blokem paměti najednou. Taková varianta se však v tomto případě
nenabízí.

Na závěr dlužno říci, že jsem byl zaskočen neefektivitou
mechanismu OpenMP Task. Domníval jsem se, že i jejich
rekurzivní tvorba bude systémem efektivně zpracovaná,
experimentální pozorování však poukázalo na vysokou závislost
hodnoty podmínky, pro kterou už se nové tasky negenerovaly.
I změna o malé jednotky měla velký vliv na celkový potřebný čas.

\subsection*{Použití programu}

Program si dokáže vygenerovat "náhodná" data nebo načíst
tensor.
\begin{verbatim}
# pro vygenerovani dat NxD
$ ./balltree <D> <N>
# pro nacteni tensoru NxD
$ ./balltree <D> <N> <path>
\end{verbatim}

Výstupem programu je poté čas nutný k přípravě struktury BallTree,
načítání dat do tohoto údaje zahrnut není. Poté budou vypsány
uzly ve formátu:
\begin{verbatim}
[ID] r = <polomer> (<souradnice stredu>)
\end{verbatim}
ID je index uzlu stromu. Platí, že kořen stromu má index \verb|i=0|,
levý potomek uzlu má vždy index \verb|2i+1|, pravý \verb|2i+2|.
Pomocí toho lze strom uložit a znovu složit bez nutnosti výpočtu.
Program očekává vstupní parametry:

\bibliography{bibliography}
\end{document}